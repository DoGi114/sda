public class View {
    private Controller controller ;

    public View(){
        controller = new Controller();
    }

    public void showJokes(int number){
        for (int i = 0; i < number ; i++) {
            System.out.println(controller.generateJoke());
        }
    }
}
