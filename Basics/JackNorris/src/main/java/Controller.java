public class Controller {

    private Model model;

    public Controller(){
        model = new Model();
    }

    public String generateJoke(){
        return model.getJoke();
    }

}
