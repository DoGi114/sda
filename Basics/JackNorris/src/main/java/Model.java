import org.json.JSONObject;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class Model {
    private final List<JSONObject> jokes;
    private DAO dao;

    public Model() {
        try {
            dao = new DAO();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        jokes = new ArrayList<>();
    }

    public String getJoke() {
        JSONObject joke;
        joke = dao.getJoke();

        if (!jokes.contains(joke)) {
            jokes.add(joke);
            return joke.getString("value");
        } else {
            return getJoke();
        }
    }
}
