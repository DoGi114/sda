package services;

import classes.Pensioner;
import dao.DefaultPensionerDAO;
import interfaces.PensionerDAO;
import interfaces.PensionerService;

import java.util.ArrayList;
import java.util.List;

public class DefaultPensionerService implements PensionerService {
    private PensionerDAO data;

    public DefaultPensionerService() {
        data = new DefaultPensionerDAO();
    }

    @Override
    public void increasePensionBy(String pesel, int percentage) {
        List<Pensioner> pensioners = new ArrayList<>();
        Pensioner pensioner = getPensioner(pesel);
        if(pensioner == null)
            return;
        pensioner.setPension(pensioner.getPension() + pensioner.getPension() * percentage / 100);
        pensioners.add(pensioner);
        data.update(pensioners);
    }

    @Override
    public Pensioner getPensioner(String pesel) {
        return data.getPensionerByPesel(pesel);
    }
}
