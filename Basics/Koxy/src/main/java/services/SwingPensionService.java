package services;

import classes.Pensioner;
import dao.DefaultPensionerDAO;
import interfaces.PensionerDAO;
import interfaces.PensionerService;

import java.util.ArrayList;
import java.util.List;

public class SwingPensionService implements PensionerService {

    private PensionerDAO data;
    private String currPesel;

    public SwingPensionService() {
        data = new DefaultPensionerDAO();
        currPesel = "";
    }

    @Override
    public void increasePensionBy(String pesel, int percentage) {
        List<Pensioner> pensioners = new ArrayList<>();
        if(currPesel.isEmpty())
            return;
        Pensioner pensioner = getPensioner(currPesel);
        if(pensioner == null)
            return;
        pensioner.setPension(pensioner.getPension() + pensioner.getPension() * percentage / 100);
        pensioners.add(pensioner);
        data.update(pensioners);
    }

    @Override
    public Pensioner getPensioner(String pesel) {
        return data.getPensionerByPesel(pesel);
    }

    public void setCurrPesel(String pesel){
        currPesel = pesel;
    }
}
