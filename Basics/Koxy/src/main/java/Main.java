import interfaces.PensionerView;
import services.SwingPensionService;
import view.DefaultPensionerView;
import view.SwingPensionerView;


public class Main {
    public static void main(String[] args) {
        PensionerView view;
        if (args.length > 0) {
            view = new DefaultPensionerView();
            String pesel = args[0];
            view.showData(pesel);
            view.increaseByPercentage(pesel, 5);
        } else {
            view = new SwingPensionerView();
        }
    }
}
