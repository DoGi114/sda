package view;

import interfaces.PensionerService;
import interfaces.PensionerView;
import services.SwingPensionService;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SwingPensionerView extends JFrame implements PensionerView {
    private JPanel container;
    private JTextArea taPesel;
    private JButton btnSearch;
    private JButton btnIncrease;
    private PensionerService ps;

    public SwingPensionerView() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("SDA - Training - Pensions");
        setMinimumSize(new Dimension(500, 100));
        setVisible(true);
        add(container);
        ps = new SwingPensionService();
        taPesel.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent documentEvent) {
                checkPeselInput();
            }

            @Override
            public void removeUpdate(DocumentEvent documentEvent) {
                checkPeselInput();
            }

            @Override
            public void changedUpdate(DocumentEvent documentEvent) {

            }

        });
        btnSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                showData(taPesel.getText());
            }
        });
        btnIncrease.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                increaseByPercentage(taPesel.getText(), 5);
                showMsg("Increased pension by 5% for: " + taPesel.getText());
            }
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        container = new JPanel();
        container.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
        container.setMinimumSize(new Dimension(120, 100));
        container.setPreferredSize(new Dimension(50, 50));
        taPesel = new JTextArea();
        taPesel.setMinimumSize(new Dimension(200, 22));
        taPesel.setOpaque(true);
        taPesel.setPreferredSize(new Dimension(200, 22));
        taPesel.setText("");
        taPesel.setToolTipText("PESEL");
        container.add(taPesel);
        btnSearch = new JButton();
        btnSearch.setEnabled(false);
        btnSearch.setText("Search");
        container.add(btnSearch);
        btnIncrease = new JButton();
        btnIncrease.setEnabled(false);
        btnIncrease.setText("Increase pension by 5%");
        container.add(btnIncrease);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return container;
    }

    @Override
    public void showData(String pesel) {
        try {
            JOptionPane.showMessageDialog(this, "Pensioner with pesel: " + pesel + " has " + ps.getPensioner(pesel).getPension());
        } catch (NullPointerException e) {
            showError("No pensioner with given PESEL.");
        }
    }

    @Override
    public void showError(String err) {
        JOptionPane.showMessageDialog(this, err);
    }

    @Override
    public void showMsg(String msg) {
        JOptionPane.showMessageDialog(this, msg);
    }

    @Override
    public void increaseByPercentage(String pesel, int percentage) {
        ps.increasePensionBy(taPesel.getText(), percentage);
    }

    private void checkPeselInput() {
        if (taPesel.getText().length() != 11) {
            btnSearch.setEnabled(false);
            btnIncrease.setEnabled(false);
        } else {
            btnSearch.setEnabled(true);
            btnIncrease.setEnabled(true);
            ((SwingPensionService) ps).setCurrPesel(taPesel.getText());
        }
    }
}
