package view;

import interfaces.PensionerService;
import interfaces.PensionerView;
import services.DefaultPensionerService;

public class DefaultPensionerView implements PensionerView {

    private PensionerService ps ;

    public DefaultPensionerView(){
        ps = new DefaultPensionerService();
    }

    @Override
    public void showData(String pesel){
        try {
            System.out.println("Pensioner with pesel: " + pesel + " has " + ps.getPensioner(pesel).getPension());
        }catch (NullPointerException e){
            showError("No pensioner with given PESEL.");
        }
    }

    @Override
    public void showError(String err){
        System.out.println(err);
    }

    @Override
    public void showMsg(String msg) {
        System.out.println(msg);
    }

    @Override
    public void increaseByPercentage(String pesel, int percentage) {
        ps.increasePensionBy(pesel, percentage);
    }
}
