package classes;

public class Pensioner {
    private final String pesel;
    private int pension;

    public Pensioner(String pesel, int pension) {
        this.pesel = pesel;
        this.pension = pension;
    }

    public String getPesel() {
        return pesel;
    }

    public int getPension() {
        return pension;
    }

    public void setPension(int pension) {
        this.pension = pension;
    }
}
