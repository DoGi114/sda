package dao;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

import classes.Pensioner;
import interfaces.PensionerDAO;

public class DefaultPensionerDAO implements PensionerDAO {
    private static final String FILE_NAME = "data.txt";
    private static final String FILE_LOCATION = "D:\\Coding\\Projects\\SDA - JAVA\\Basics\\Koxy\\";

//    private ArrayList<Pensioner> data;

    public DefaultPensionerDAO() {
//        data = new ArrayList<Pensioner>();
    }

    @Override
    public List<Pensioner> getAllPensioners() {
        String[] s;
        File file = new File(FILE_LOCATION + FILE_NAME);
        List<Pensioner> pensioners = new ArrayList<>();

        Scanner scnr = null;
        try {
            scnr = new Scanner(file);

            while (scnr.hasNextLine()) {
                String line = scnr.nextLine();
                s = line.split(";");
                if (s.length != 2) continue;
                pensioners.add(new Pensioner(s[0], Integer.parseInt(s[1])));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return pensioners;
    }

    @Override
    public Pensioner getPensionerByPesel(String pesel) {
        List<Pensioner> pensioners = getAllPensioners();
        for (Pensioner pensioner : pensioners) {
            if (pensioner.getPesel().equals(pesel)) {
                return pensioner;
            }
        }
        return null;
    }

    @Override
    public void update(List<Pensioner> pensioners) {
        List<Pensioner> pensionersFromFile = getAllPensioners();

        updateList(pensioners, pensionersFromFile);

        uploadToFile(pensionersFromFile);
    }

    private void updateList(List<Pensioner> pensioners, List<Pensioner> pensionersFromFile){
        for(Pensioner pensioner : pensioners){
            for(Pensioner pensionerFromFile : pensionersFromFile){
                if(pensioner.getPesel().equals(pensionerFromFile.getPesel())){
                    pensionersFromFile.set(pensionersFromFile.indexOf(pensionerFromFile),pensioner);
                }
            }
        }

    }

    private void uploadToFile(List<Pensioner> pensioners){
        File file = new File(FILE_LOCATION + FILE_NAME);
        String[] s;
        try (FileWriter fw = new FileWriter(file)) {
            for (Pensioner entry : pensioners) {
                fw.write(entry.getPesel() + ";" + entry.getPension() + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
