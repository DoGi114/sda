package interfaces;

import classes.Pensioner;

import java.util.List;

public interface PensionerDAO {
    void update(List<Pensioner> pensioners) ;
    List<Pensioner> getAllPensioners();
    Pensioner getPensionerByPesel(String pesel);
}
