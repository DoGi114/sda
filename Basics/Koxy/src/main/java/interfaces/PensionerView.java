package interfaces;

public interface PensionerView {
    void showData(String pesel);
    void showError(String err);
    void showMsg(String msg);
    void increaseByPercentage(String pesel, int percentage);
}
