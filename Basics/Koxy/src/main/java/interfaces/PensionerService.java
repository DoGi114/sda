package interfaces;

import classes.Pensioner;

import java.math.BigDecimal;

public interface PensionerService {
    Pensioner getPensioner(String pesel);
    void increasePensionBy(String pesel, int percentage);
}
