public class Language {
    private String lang;
    private String shortLang;

    public Language(String shortLang){
        this.shortLang = shortLang;
        this.lang = shortLangToLang(shortLang);
    }

    private String shortLangToLang(String shortLang){
        String res = "";

        switch (shortLang){
            case "aa":
                res = "Afar";
                break;
            case "ab":
                res = "Abkhazian";
                break;
            case "af":
                res = "Afrikaans";
                break;
            case "ak":
                res = "Akan";
                break;
            case "am":
                res = "Amharic";
                break;
            case "ar":
                res = "Arabic";
                break;
            case "as":
                res = "Assamese";
                break;
            case "ay":
                res = "Aymara";
                break;
            case "az":
                res = "Azerbaijani";
                break;
            case "ba":
                res = "Bashkir";
                break;
            case "be":
                res = "Belarusian";
                break;
            case "bg":
                res = "Bulgarian";
                break;
            case "bh":
                res = "Bihari";
                break;
            case "bi":
                res = "Bislama";
                break;
            case "bn":
                res = "Bengali";
                break;
            case "bo":
                res = "Tibetan";
                break;
            case "br":
                res = "Breton";
                break;
            case "bs":
                res = "Bosnian";
                break;
            case "bug":
                res = "Buginese";
                break;
            case "ca":
                res = "Catalan";
                break;
            case "ceb":
                res = "Cebuano";
                break;
            case "chr":
                res = "Cherokee";
                break;
            case "co":
                res = "Corsican";
                break;
            case "crs":
                res = "Seselwa";
                break;
            case "cs":
                res = "Czech";
                break;
            case "cy":
                res = "Welsh";
                break;
            case "da":
                res = "Danish";
                break;
            case "de":
                res = "German";
                break;
            case "dv":
                res = "Dhivehi";
                break;
            case "dz":
                res = "Dzongkha";
                break;
            case "egy":
                res = "Egyptian";
                break;
            case "el":
                res = "Greek";
                break;
            case "en":
                res = "English";
                break;
            case "eo":
                res = "Esperanto";
                break;
            case "es":
                res = "Spanish";
                break;
            case "et":
                res = "Estonian";
                break;
            case "eu":
                res = "Basque";
                break;
            case "fa":
                res = "Persian";
                break;
            case "fi":
                res = "Finnish";
                break;
            case "fj":
                res = "Fijian";
                break;
            case "fo":
                res = "Faroese";
                break;
            case "fr":
                res = "French";
                break;
            case "fy":
                res = "Frisian";
                break;
            case "ga":
                res = "Irish";
                break;
            case "gd":
                res = "Scots Gaelic";
                break;
            case "gl":
                res = "Galician";
                break;
            case "gn":
                res = "Guarani";
                break;
            case "got":
                res = "Gothic";
                break;
            case "gu":
                res = "Gujarati";
                break;
            case "gv":
                res = "Manx";
                break;
            case "ha":
                res = "Hausa";
                break;
            case "haw":
                res = "Hawaiian";
                break;
            case "hi":
                res = "Hindi";
                break;
            case "hmn":
                res = "Hmong";
                break;
            case "hr":
                res = "Croatian";
                break;
            case "ht":
                res = "Haitian Creole";
                break;
            case "hu":
                res = "Hungarian";
                break;
            case "hy":
                res = "Armenian";
                break;
            case "ia":
                res = "Interlingua";
                break;
            case "id":
                res = "Indonesian";
                break;
            case "ie":
                res = "Interlingue";
                break;
            case "ig":
                res = "Igbo";
                break;
            case "ik":
                res = "Inupiak";
                break;
            case "is":
                res = "Icelandic";
                break;
            case "it":
                res = "Italian";
                break;
            case "iu":
                res = "Inuktitut";
                break;
            case "iw":
                res = "Hebrew";
                break;
            case "ja":
                res = "Japanese";
                break;
            case "jw":
                res = "Javanese";
                break;
            case "ka":
                res = "Georgian";
                break;
            case "kha":
                res = "Khasi";
                break;
            case "kk":
                res = "Kazakh";
                break;
            case "kl":
                res = "Greenlandic";
                break;
            case "km":
                res = "Khmer";
                break;
            case "kn":
                res = "Kannada";
                break;
            case "ko":
                res = "Korean";
                break;
            case "ks":
                res = "Kashmiri";
                break;
            case "ku":
                res = "Kurdish";
                break;
            case "ky":
                res = "Kyrgyz";
                break;
            case "la":
                res = "Latin";
                break;
            case "lb":
                res = "Luxembourgish";
                break;
            case "lg":
                res = "Ganda";
                break;
            case "lif":
                res = "Limbu";
                break;
            case "ln":
                res = "Lingala";
                break;
            case "lo":
                res = "Laothian";
                break;
            case "lt":
                res = "Lithuanian";
                break;
            case "lv":
                res = "Latvian";
                break;
            case "mfe":
                res = "Mauritian Creole";
                break;
            case "mg":
                res = "Malagasy";
                break;
            case "mi":
                res = "Maori";
                break;
            case "mk":
                res = "Macedonian";
                break;
            case "ml":
                res = "Malayalam";
                break;
            case "mn":
                res = "Mongolian";
                break;
            case "mr":
                res = "Marathi";
                break;
            case "ms":
                res = "Malay";
                break;
            case "mt":
                res = "Maltese";
                break;
            case "my":
                res = "Burmese";
                break;
            case "na":
                res = "Nauru";
                break;
            case "ne":
                res = "Nepali";
                break;
            case "nl":
                res = "Dutch";
                break;
            case "no":
                res = "Norwegian";
                break;
            case "nr":
                res = "Ndebele";
                break;
            case "nso":
                res = "Pedi";
                break;
            case "ny":
                res = "Nyanja";
                break;
            case "oc":
                res = "Occitan";
                break;
            case "om":
                res = "Oromo";
                break;
            case "or":
                res = "Oriya";
                break;
            case "pa":
                res = "Punjabi";
                break;
            case "pl":
                res = "Polish";
                break;
            case "ps":
                res = "Pashto";
                break;
            case "pt":
                res = "Portuguese";
                break;
            case "qu":
                res = "Quechua";
                break;
            case "rm":
                res = "Rhaeto Romance";
                break;
            case "rn":
                res = "Rundi";
                break;
            case "ro":
                res = "Romanian";
                break;
            case "ru":
                res = "Russian";
                break;
            case "rw":
                res = "Kinyarwanda";
                break;
            case "sa":
                res = "Sanskrit";
                break;
            case "sco":
                res = "Scots";
                break;
            case "sd":
                res = "Sindhi";
                break;
            case "sg":
                res = "Sango";
                break;
            case "si":
                res = "Sinhalese";
                break;
            case "sk":
                res = "Slovak";
                break;
            case "sl":
                res = "Slovenian";
                break;
            case "sm":
                res = "Samoan";
                break;
            case "sn":
                res = "Shona";
                break;
            case "so":
                res = "Somali";
                break;
            case "sq":
                res = "Albanian";
                break;
            case "sr":
                res = "Serbian";
                break;
            case "ss":
                res = "Siswant";
                break;
            case "st":
                res = "Sesotho";
                break;
            case "su":
                res = "Sundanese";
                break;
            case "sv":
                res = "Swedish";
                break;
            case "sw":
                res = "Swahili";
                break;
            case "syr":
                res = "Syriac";
                break;
            case "ta":
                res = "Tamil";
                break;
            case "te":
                res = "Telugu";
                break;
            case "tg":
                res = "Tajik";
                break;
            case "th":
                res = "Thai";
                break;
            case "ti":
                res = "Tigrinya";
                break;
            case "tk":
                res = "Turkmen";
                break;
            case "tl":
                res = "Tagalog";
                break;
            case "tlh":
                res = "Klingon";
                break;
            case "tn":
                res = "Tswana";
                break;
            case "to":
                res = "Tonga";
                break;
            case "tr":
                res = "Turkish";
                break;
            case "ts":
                res = "Tsonga";
                break;
            case "tt":
                res = "Tatar";
                break;
            case "ug":
                res = "Uighur";
                break;
            case "uk":
                res = "Ukrainian";
                break;
            case "ur":
                res = "Urdu";
                break;
            case "uz":
                res = "Uzbek";
                break;
            case "ve":
                res = "Venda";
                break;
            case "vi":
                res = "Vietnamese";
                break;
            case "vo":
                res = "Volapuk";
                break;
            case "war":
                res = "Waray Philippines";
                break;
            case "wo":
                res = "Wolof";
                break;
            case "xh":
                res = "Xhosa";
                break;
            case "yi":
                res = "Yiddish";
                break;
            case "yo":
                res = "Yoruba";
                break;
            case "za":
                res = "Zhuang";
                break;
            case "zh":
                res = "Chinese Simplified";
                break;
            case "zh-Hant":
                res = "Chinese Traditional";
                break;
            case "zu":
                res = "Zulu";
                break;
        }

        return res;
    }

    public String getLang() {
        return lang;
    }

    public String getShortLang() {
        return shortLang;
    }
}
