import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class TextFile {
    private String path;
    private Language lang;

    public TextFile(String path){
        LanguageDetector languageDetector = new LanguageDetector();
        this.path = path;
        this.lang = new Language(languageDetector.detectLanguage(getTextFromFile(new File(path))));
    }

    private String getTextFromFile(File file){
        StringBuilder res = new StringBuilder();
        List<String > lines;
        try {
            lines = Files.readAllLines(file.getAbsoluteFile().toPath());
            for(String line : lines){
                res.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res.toString();
    }

    public String getPath() {
        return path;
    }

    public Language getLang() {
        return lang;
    }
}
