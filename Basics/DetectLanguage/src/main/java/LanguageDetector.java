import com.detectlanguage.DetectLanguage;
import com.detectlanguage.errors.APIError;

public class LanguageDetector {
    private final String API_KEY = "a8adc0c54cb2dc2b766b4f8a40cffc3e";

    public LanguageDetector(){
        DetectLanguage.apiKey = API_KEY;
    }

    public String detectLanguage(String text) {
        try {
            return DetectLanguage.simpleDetect(text);
        } catch (APIError apiError) {
            apiError.printStackTrace();
            return "Api error";
        }
    }
}
