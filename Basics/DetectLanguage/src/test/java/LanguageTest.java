import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
public class LanguageTest {

    @Test
    void languageTest(){
        Language language = new Language("pl");
        assertEquals(language.getLang(), "Polish");
        assertEquals(language.getShortLang(), "pl");

        language = new Language("en");
        assertEquals(language.getLang(), "English");
        assertEquals(language.getShortLang(), "en");
    }
}
