import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TextFileTest {

    @Test
    void doesPathExist(){
        String resourceName = "TextFileTestEnglish.txt";

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(resourceName)).getFile());
        String absolutePath = file.getAbsolutePath();

        assertTrue(absolutePath.endsWith("TextFileTestEnglish.txt"));

        resourceName = "TextFileTestPolish.txt";
        file = new File(Objects.requireNonNull(classLoader.getResource(resourceName)).getFile());
        absolutePath = file.getAbsolutePath();

        assertTrue(absolutePath.endsWith("TextFileTestPolish.txt"));
    }

    @Test
    void createTextFile(){
        String resourceName = "TextFileTestPolish.txt";
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(resourceName)).getFile());
        String absolutePath = file.getAbsolutePath();
        TextFile file1 = new TextFile(absolutePath.replace("%20", " "));
        assertEquals(file1.getLang().getShortLang(), "pl");
        assertEquals(file1.getPath(), absolutePath.replace("%20", " "));

        resourceName = "TextFileTestEnglish.txt";
        classLoader = getClass().getClassLoader();
        file = new File(Objects.requireNonNull(classLoader.getResource(resourceName)).getFile());
        absolutePath = file.getAbsolutePath();
        TextFile file2 = new TextFile(absolutePath.replace("%20", " "));
        assertEquals(file2.getLang().getShortLang(), "en");
        assertEquals(file2.getPath(), absolutePath.replace("%20", " "));
    }
}
