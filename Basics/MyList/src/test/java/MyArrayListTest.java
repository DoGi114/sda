import implementations.MyArrayList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class MyArrayListTest {
    @Test
    void testAdd(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        assertNotNull(mList.get(0));
    }

    @Test
    void testExpanding(){
        MyArrayList<String> mList = new MyArrayList<>();
        for(int i = 0; i < 15; i++){
            mList.add(String.valueOf(i));
        }
        assertEquals(mList.length(), 15);
    }

    @Test
    void testGet(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        assertEquals(mList.getIndex("Test"), 0);
    }

    @Test
    void testLength(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        assertEquals(mList.length(), 1);
    }

    @Test
    void testRemove(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        mList.add("Test2");
        mList.add("Test3");
        mList.removeAt(0);
        mList.remove("Test2");
        assertEquals(mList.length(), 1);
    }

    @Test
    void testClear(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        mList.add("Test2");
        mList.add("Test3");
        mList.clear();
        assertEquals(mList.length(), 0);
    }

    @Test
    void testToString(){
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("Test");
        assertEquals(mList.toString(), "[Test]");
    }

}
