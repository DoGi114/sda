import implementations.MyLinkedList;
import implementations.MyLinkedList;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MyLinkedListTest {

    @Test
    void testAdd(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        assertNotNull(mList.get(0));
    }

    @Test
    void testExpanding(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        for(int i = 0; i < 15; i++){
            mList.add(String.valueOf(i));
        }
        assertEquals(mList.length(), 16);
    }

    @Test
    void testRoot(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        assertEquals(mList.getRoot(), "root");
        mList.setRoot("newRoot");
        assertEquals(mList.getRoot(), "newRoot");
    }

    @Test
    void testGet(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        assertEquals(mList.getIndex("Test"), 1);
        assertEquals(mList.get(1), "Test");
    }

    @Test
    void testLength(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        assertEquals(mList.length(), 2);
    }

    @Test
    void testRemove(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        mList.add("Test2");
        mList.add("Test3");
        mList.add("Test4");
        mList.add("Test5");
        mList.removeAt(0);
        mList.removeAt(4);
        mList.remove("Test2");
        assertEquals(mList.length(), 3);
    }

    @Test
    void testClear(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        mList.add("Test2");
        mList.add("Test3");
        mList.clear();
        assertEquals(mList.length(), 0);
    }

    @Test
    void testToString(){
        MyLinkedList<String> mList = new MyLinkedList<>("root");
        mList.add("Test");
        assertEquals(mList.toString(), "[root,Test]");
    }
}
