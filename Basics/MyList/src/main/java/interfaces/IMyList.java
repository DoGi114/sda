package interfaces;

public interface IMyList<T> {
    void add(T o);
    void remove(T o);
    void removeAt(int idx);
    T get(int idx);
    int getIndex(T o);
    String toString();
    int length();
    void clear();

}
