package classes;

public class MyNode<T> {
    private MyNode<T> nextNode;
    private MyNode<T> prevNode;
    private T item;

    public MyNode(final T item) {
        this.item = item;
        this.nextNode = null;
        this.prevNode = null;
    }

    public MyNode<T> getNextNode() {
        return this.nextNode;
    }

    public void setNextNode(final MyNode<T> nextNode) {
        this.nextNode = nextNode;
    }

    public MyNode<T> getPrevNode() {
        return this.prevNode;
    }

    public void setPrevNode(final MyNode<T> prevNode) {
        this.prevNode = prevNode;
    }

    public T getItem() {
        return this.item;
    }

    public void setItem(final T item) {
        this.item = item;
    }

    public boolean hasNext(){
        return nextNode != null;
    }

    public boolean hasPrev(){
        return prevNode != null;
    }
}
