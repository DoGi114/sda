package classes;

public class TestObject {
    private String s;
    private int id;

    public TestObject(String s, int id) {
        this.s = s;
        this.id = id;
    }

    public String getS() {
        return s;
    }

    public void setS(String s) {
        this.s = s;
    }
}
