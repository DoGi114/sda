import classes.TestObject;
import implementations.MyArrayList;
import implementations.MyLinkedList;

public class Main {
    public static void main(String[] args) {
        MyArrayList<String> mList = new MyArrayList<>();
        mList.add("lololo");
        mList.add("tototo");
        mList.add("Damian");
        mList.add("Nguyen");
        System.out.println(mList.get(3));
        System.out.println(mList);

        MyLinkedList<TestObject> mLinkedList = new MyLinkedList<>(new TestObject("0", 0));
        mLinkedList.add(new TestObject("1", 1));
        mLinkedList.add(new TestObject("2", 2));
        mLinkedList.add(new TestObject("3", 3));
        mLinkedList.add(new TestObject("4", 4));
        mLinkedList.setRoot(new TestObject("root", -1));
        TestObject t = new TestObject("5", 5);
        mLinkedList.add(t);
        System.out.println("3rd item: " + mLinkedList.get(3).getS());
//        mLinkedList.get(3).setS("Damian");
        System.out.println("3rd item 2: " + mLinkedList.get(3).getS());
        System.out.println("Get t idx: " + mLinkedList.getIndex(t));
        System.out.println("LinkedList length: " + mLinkedList.length());
        mLinkedList.removeAt(1);
        System.out.println("1st item: " + mLinkedList.get(0).getS());
        System.out.println("1st item: " + mLinkedList.get(1).getS());
        System.out.println("1st item: " + mLinkedList.get(2).getS());
        System.out.println("LinkedList length: " + mLinkedList.length());


        mLinkedList.clear();
        System.out.println("LinkedList length: " + mLinkedList.length());
    }
}
