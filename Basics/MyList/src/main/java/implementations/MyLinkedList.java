package implementations;

import classes.MyNode;
import interfaces.IMyList;

public class MyLinkedList<T> implements IMyList<T> {

    private MyNode<T> node;
    private MyNode<T> root;

    public MyLinkedList(T root){
        this.root = new MyNode<>(root);
    }

    public void add(T o) {
        MyNode<T> newNode = new MyNode<>(o);
        node = root;
        while(node.hasNext()){
            node = node.getNextNode();
        }

        node.setNextNode(newNode);
        newNode.setPrevNode(node);
    }

    @Override
    public void remove(T o) {
        node = root;
        while(node.hasNext() && !node.getItem().equals(o)){
            node = node.getNextNode();
        }
        MyNode<T> prevNode = node.getPrevNode();
        MyNode<T> nextNode = node.getNextNode();
        node.getPrevNode().setNextNode(nextNode);
        if(node.getNextNode() != null)
            node.getNextNode().setPrevNode(prevNode);
        node = null;
    }

    @Override
    public void removeAt(int idx) {
        int i = 0;
        node = root;
        while(node.hasNext()  && i != idx){
            node = node.getNextNode();
            i++;
        }

        MyNode<T> prevNode = node.getPrevNode();
        MyNode<T> nextNode = node.getNextNode();

        if(node.equals(root)) {
            root = nextNode;
        }

        if(node.hasPrev()) {
            node.getPrevNode().setNextNode(nextNode);
        }
        if(node.getNextNode() != null) {
            node.getNextNode().setPrevNode(prevNode);
        }
        node = null;
    }

    @Override
    public T get(int idx) {
        int i = 0;
        node = root;
        while(node.hasNext() && i != idx){
            node = node.getNextNode();
            i++;
        }

        return node.getItem();
    }

    @Override
    public int getIndex(T o) {
        int res = 0;

        node = root;

        while(node.hasNext() && !node.getItem().equals(o)){
            node = node.getNextNode();
            res++;
        }

        return res;
    }

    @Override
    public int length() {
        int res;

        if(root != null){
            res = 1;
        }else{
            return 0;
        }

        node = root;
        while(node.hasNext()){
            node = node.getNextNode();
            res++;
        }

        return res;
    }

    @Override
    public void clear() {
        MyNode<T> prevNode;
        node = root;
        while(node.hasNext()){
            prevNode = node;
            node = node.getNextNode();
            node.setPrevNode(null);
            prevNode.setNextNode(null);
        }
        root = null;
    }

    public T getRoot() {
        return root.getItem();
    }

    public void setRoot(T root) {
        MyNode<T> nextNode = this.root.getNextNode();
        this.root = new MyNode<>(root);
        this.root.setNextNode(nextNode);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[");

        sb.append(root.getItem());
        sb.append(",");

        node = root;

        while(node.hasNext()){
            node = node.getNextNode();
            sb.append(node.getItem());
            sb.append(",");
        }

        if (sb.toString().endsWith(",")) {
            sb.deleteCharAt(sb.toString().length() - 1);
        }

        sb.append("]");
        return sb.toString();
    }
}
