package implementations;

import interfaces.IMyList;

public class MyArrayList<T> implements IMyList<T> {
    private T[] arr;
    private int length;

    public MyArrayList() {
        arr = (T[]) new Object[10];
    }

    public void add(T o) {
        T[] newArr;

        if (length == arr.length) {
            length = 0;
            newArr = (T[]) new Object[arr.length * 2];
            for (int j = 0; j < arr.length; j++) {
                newArr[j] = arr[j];
                length++;
            }

            arr = newArr;
        }

        arr[length] = o;
        length++;
    }

    public void remove(T o) {
        for (int i = 0; i < arr.length; i++) {
            if (o.equals(arr[i])) {
                arr[i] = null;
                break;
            }
        }
        length--;
    }

    public void removeAt(int idx) {
        arr[idx] = null;
        length--;
    }

    public T get(int idx) {
        return arr[idx];
    }

    public int getIndex(T o) {
        int idx = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].equals(o)) {
                idx = i;
                break;
            }
        }
        return idx;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[");

        for (T o : arr) {
            if (o == null)
                continue;
            sb.append(o);
            sb.append(",");
        }

        if (sb.toString().endsWith(",")) {
            sb.deleteCharAt(sb.toString().length() - 1);
        }

        sb.append("]");
        return sb.toString();
    }

    public int length() {
        return length;
    }

    public void clear() {
        arr = (T[]) new Object[10];
        length = 0;
    }
}
