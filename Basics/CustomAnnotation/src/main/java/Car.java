import java.awt.*;

public class Car {
    private boolean isMoving;
    private int velocity;
    private String type;
    private String model;
    private String brand;
    public Color color;

    public Car(final boolean isMoving, final int velocity, final String type, final String model, final String brand, final Color color) {
        this.isMoving = isMoving;
        this.velocity = velocity;
        this.type = type;
        this.model = model;
        this.brand = brand;
        this.color = color;
    }

    public Car() {

    }

    public Car(final String type, final String model, final String brand) {
        this.type = type;
        this.model = model;
        this.brand = brand;
    }

    @SuperMethod(run = true)
    public void drive(final int velocity){
        this.isMoving = true;
        this.velocity = velocity;
    }

    public void stop(){
        this.isMoving = false;
        this.velocity = 0;
    }

    public void repaint(final Color color){
        this.color = color;
    }

    public boolean isMoving() {
        return this.isMoving;
    }

    public int getVelocity() {
        return this.velocity;
    }

    public String getType() {
        return this.type;
    }

    public String getModel() {
        return this.model;
    }

    public String getBrand() {
        return this.brand;
    }

    public Color getColor(){
        return this.color;
    }
}
