package interfaces;

import java.util.Date;

public interface ControllerInterface {

    double getAverage(String currencyCode);

    double getBids(String currencyCode);

    double getAsks(String currencyCode);

    double calculate(double money, String currencyCode);

    double calculateProfit(double money, Date date, String currencyCode);
}
