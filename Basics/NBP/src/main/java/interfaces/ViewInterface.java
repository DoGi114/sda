package interfaces;

import java.util.Date;

public interface ViewInterface {
    void showAverage(String currencyCode);

    void showBid(String currencyCode);

    void showAsks(String currencyCode);

    void calculate(double money, String currencyCode);

    void showMonthlyProfit(double money, Date date, String currencyCode);
}
