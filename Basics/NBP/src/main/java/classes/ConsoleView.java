package classes;

import interfaces.ViewInterface;

import java.util.Date;

public class ConsoleView implements ViewInterface {

    private final ConsoleController consoleController;

    public ConsoleView(){
        consoleController = new ConsoleController();
    }

    public void showAverage(String currencyCode){
        System.out.println("Current average price for " + currencyCode + " is " + String.format("%.2f", consoleController.getAverage(currencyCode)));
    }

    public void showBid(String currencyCode){
        System.out.println("Current bid price for " + currencyCode + " is " + String.format("%.2f", consoleController.getBids(currencyCode)));
    }

    public void showAsks(String currencyCode){
        System.out.println("Current ask price for " + currencyCode + " is " + String.format("%.2f", consoleController.getAsks(currencyCode)));
    }

    public void calculate(double money, String currencyCode){
        System.out.println("You bought " + String.format("%.2f", consoleController.calculate(money, currencyCode)) + " " + currencyCode + " for " + String.format("%.2f", money) + " PLN.");
    }

    public void showMonthlyProfit(double money, Date date, String currencyCode){
        System.out.println("You'd earn " + String.format("%.2f", consoleController.calculateProfit(money, date, currencyCode)) + " PLN if you bought " + currencyCode + " for " + String.format("%.2f", money) + " PLN on " + date + ".");
    }
}
