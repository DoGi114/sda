package classes;

import java.util.Calendar;
import java.util.Date;

public class Main {
    public static void main(String[] args) {
        //Calculate from PLN to USD,EUR,GBP,CHF
        ConsoleView view = new ConsoleView();
        view.showAsks("USD");
        view.showAverage("USD");
        view.showBid("USD");
        view.calculate(100, "USD");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        view.showMonthlyProfit(100, cal.getTime(), "USD");
    }
}
