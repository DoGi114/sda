package classes;

import interfaces.ControllerInterface;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConsoleController implements ControllerInterface {
    private final Model model;

    public ConsoleController(){
        model = new Model();
    }

    public double getAverage(String currencyCode){
        return model.getCurrency(currencyCode).getAverage();
    }

    public double getBids(String currencyCode){
        return model.getCurrency(currencyCode).getBid();
    }

    public double getAsks(String currencyCode) {
        return model.getCurrency(currencyCode).getAsks();
    }

    public double calculate(double money, String currencyCode) {
        Currency currency = model.getCurrency(currencyCode);
        return money / currency.getAsks();
    }

    public double calculateProfit(double money, Date date, String currencyCode) {
        Currency currency = model.getCurrency(currencyCode);
        Currency currencyPrevious = model.getCurrencyFromDate(currencyCode, date);

       return (money / currencyPrevious.getAsks() * currency.getBid()) - money;
//        asks miesiac temu i bid teraz
    }
}
