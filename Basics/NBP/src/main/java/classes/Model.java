package classes;

import org.json.JSONObject;

import java.util.Date;

public class Model {
    private final DAO dao;

    public Model() {
        dao = new DAO();
    }

    public Currency getCurrency(String currencyCode) {
        JSONObject tableA = dao.getTableACurrency(currencyCode);
        JSONObject tableC = dao.getTableCCurrency(currencyCode);
        JSONObject tableAData = tableA.getJSONArray("rates").getJSONObject(0);
        JSONObject tableCData = tableC.getJSONArray("rates").getJSONObject(0);
        Currency currency = new Currency(tableA.getString("currency"), tableA.getString("code"));
        currency.setAverage(tableAData.getDouble("mid"));
        currency.setAsks(tableCData.getDouble("ask"));
        currency.setBid(tableCData.getDouble("bid"));
        return currency;
    }

    public Currency getCurrencyFromDate(String currencyCode, Date date) {
        JSONObject tableA = dao.getTableACurrencyFromDate(currencyCode, date);
        JSONObject tableC = dao.getTableCCurrencyFromDate(currencyCode, date);
        JSONObject tableAData = tableA.getJSONArray("rates").getJSONObject(0);
        JSONObject tableCData = tableC.getJSONArray("rates").getJSONObject(0);
        Currency currency = new Currency(tableA.getString("currency"), tableA.getString("code"));
        currency.setAverage(tableAData.getDouble("mid"));
        currency.setAsks(tableCData.getDouble("ask"));
        currency.setBid(tableCData.getDouble("bid"));
        return currency;
    }
}
