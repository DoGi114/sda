package classes;

public class Currency {
    private final String name;
    private final String code;
    private double bid;
    private double asks;
    private double average;

    public Currency(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public String getCode() {
        return code;
    }

    public double getBid() {
        return bid;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public double getAsks() {
        return asks;
    }

    public void setAsks(double asks) {
        this.asks = asks;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
