package com.sda.basic.interfaces;

public interface Engine {
    public void drive(double velocity);
}
