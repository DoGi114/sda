package com.sda.basic.classes;

public class MyNumber {

    private double number;

    public MyNumber(double number){
        this.number = number;
    }

    public MyNumber add(MyNumber a){
        return new MyNumber(number+a.getNumber());
    }

    public double getNumber() {
        return number;
    }

    public boolean isOdd(){
        return number % 2 != 0;
    }

    public boolean isEven(){
        return !isOdd();
    }

    public MyNumber pow(MyNumber a){
        return new MyNumber(Math.pow(number, a.getNumber()));
    }

    public MyNumber substract(MyNumber a){
        return new MyNumber(number - a.getNumber());
    }

    public String toString(){
        return String.valueOf(number);
    }
}
