package com.sda.basic.classes;

import com.sda.basic.interfaces.Engine;

public class Vehicle implements Engine {

    private double velocity;

    public Vehicle(){

    };

    @Override
    public void drive(double velocity) {
        this.velocity = velocity;
    }

    public double getVelocity() {
        return velocity;
    }
}
