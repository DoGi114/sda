package com.sda.basic;

import com.sda.basic.classes.Bike;
import com.sda.basic.classes.Car;
import com.sda.basic.classes.MyNumber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Main {

    private static void set1(){
        System.out.println("pow(3,2) = " + pow(3, 2));
        System.out.println("pow(2,3) = " + pow(2, 3));
        System.out.println("multiply(2, 3, 6) = " + multiply(2, 3, 6));
        System.out.println("multiply(1, 2, 2, 8) = " + multiply(1, 2, 2, 8));
        System.out.println("sumOfDigits(45) = " + sumOfDigits(45));
        System.out.println("sumOfDigits(28) = " + sumOfDigits(28));
        System.out.println("sumOfDigits(11) = " + sumOfDigits(11));
        printAmericanFlag();
        System.out.println("isBigger(5, 1) = " + isBigger(5, 1));
        System.out.println("isBigger(6,6) = " + isBigger(6, 6));
        System.out.println("silnia(3) = " + silnia(3));
        System.out.println("silnia(4) = " + silnia(4));
        System.out.println("silnia(5) = " + silnia(5));
        System.out.println("calculateSecondsToTime(86399) = " + calculateSecondsToTime(86399));
        int[] numbers1 = {1, 45, 564, 7867, 34, 5456, 210};
        printGratedThenHundred(numbers1);
        int[] numbers2 = {1, 45, 564, 7867, 34, 5456, 210};
        printOddNumbers(numbers2);
    }

    public static void set2(){

        int[] numbers3 = {1, 45, 564, 7867, 34, 5456, 210};
        int[] revNumbers3 = reverseTable(numbers3);
        float[] numbers4 = {(float) 2.0, (float) 3.0, (float) 5.0};
        System.out.println("average(numbers4) = " + average(numbers4));
        int[] numbers5 = {2, 3, 5};
        System.out.println("doesArrayContainsValue(numbers5, 2) = " + doesArrayContainsValue(numbers5, 2));
        System.out.println("doesArrayContainsValue(numbers5, 9) = " + doesArrayContainsValue(numbers5, 9));

        int[] numbers6 = {2, 3, 5};
        System.out.println("getIndex(numbers6, 2) = " + getIndex(numbers6, 2));
        System.out.println("getIndex(numbers6, 5) = " + getIndex(numbers6, 5));
        System.out.println("getIndex(numbers6, 9) = " + getIndex(numbers6, 9));
        int[] numbers7 = {2, 3, 5};
        System.out.println("maxInArray(liczby)= " + maxInArray(numbers7));
        String[] strings = {"aaaa", "bbbb", "ccccc"};
        System.out.println("reverseTableOfStrings(strings) = " + Arrays.toString(reverseTableOfStrings(strings)));
        int[] numbers8 = {2, 3, 5, 56, 87, 12, 9, 2};
        printHalfTable(numbers8);
        int[] numbers9 = {2, 3, 5};
        int[] numbers10 = {7, 10, 0};
        System.out.println("sumOfTables(numbers9, numbers10) = " + Arrays.toString(sumOfTables(numbers9, numbers10)));
        System.out.println("sameArrays(numbers11, numbers12) = " + sameArrays(numbers9, numbers10));
        int[] numbers11 = {2, 3, 5};
        int[] numbers12 = {2, 3, 5};
        System.out.println("sameArrays(numbers11, numbers12) = " + sameArrays(numbers11, numbers12));
        int[] numbers13 = {2, 3, 5};
        int[] numbers14 = {2, 3, 5, 7};
        System.out.println("sameArrays(numbers13, numbers14) = " + sameArrays(numbers13, numbers14));
        System.out.println("isPrime(1) = " + isPrime(1));
        System.out.println("isPrime(-1) = " + isPrime(-1));
        System.out.println("isPrime(0) = " + isPrime(0));
        System.out.println("isPrime(5) = " + isPrime(5));
        System.out.println("isPrime(6) = " + isPrime(6));
        int[] numbers15 = {2, 3, 5, 7, 10, 3, 5};
        arrayDuplicates(numbers15);
    }

    public static void set3(){
        int[] numbers15 = {2, 3, 5, 7, 10, 3, 5};
        write20();
        writeOdds();
        write20v2();
        printArray(4);
        printArrayv2(4);
        printArrayv3(4);
        printArrayv4(4);
        System.out.println("equation(5) = " + equation(5));
        System.out.println("reverseString(\"domek\") = " + reverseString("domek"));
        showArray(numbers15);
        writeFromTheEnd(123);
        System.out.println("isPolyndrom(\"kajak\") = " + isPolyndrom("kajak"));
        System.out.println("isPolyndrom(\"kotek\") = " + isPolyndrom("kotek"));
    }

    private static void set4(){
        set4Task1();
        set4Task2();
        set4Task3();
        set4Task4();
        set4Task5();
    }

    public static void main(String[] args) {
        set1();
        set2();
        set3();
        set4();
    }

    //Set number 1

    private static long pow(int num, int idx){
        long res = 1;
        for(int i = 1; i <= idx; i++){
            res *= num;
        }
        return res;
    }

    private static int multiply(int... args){
        int res = 1;

        for (int arg : args) {
            res *= arg;
        }
        return res;
    }

    private static int sumOfDigits(int num){
        int res = 0;
        while(num>0) {
            res += num % 10;
            num/=10;
        }
        return res;
    }

    private static void printAmericanFlag(){
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println(" * * * * *  ==================================");
        System.out.println("* * * * * * ==================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
        System.out.println("==============================================");
    }

    private static boolean isBigger(int a, int b){
        return a >= b;
    }

    private static int silnia(int num){
        int res = 1;

        for(int i = 1; i <= num; i++){
            res *= i;
        }

        return res;
    }

    private static String calculateSecondsToTime(int sec){
        int hours = sec / 3600;
        int minutes = (sec % 3600) / 60;
        int seconds = sec % 60;

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    private static void printGratedThenHundred(int[] nums){
        System.out.println("Greater than hundred:");
        for(int num: nums) {
            if(num > 100)
                System.out.println(num);
        }
    }

    private static void printOddNumbers(int[] nums){
        System.out.println("Odd numbers:");
        for(int num: nums) {
            if(num % 2 == 0)
                System.out.println(num);
        }
    }

    private static int[] reverseTable(int[] nums){
        int length = nums.length;
        int[] res = new int[length];

        for(int i = 0; i < length; i++){
            res[length-i-1] = nums[i];
        }

        return res;
    }

    //Set number 2

    private static float average(float[] nums){
        int counter = nums.length;
        float sum = 0;
        for(float num : nums){
            sum += num;
        }
        return sum/counter;
    }

    private static boolean doesArrayContainsValue(int[] nums, int num ){
        for (int x : nums){
            if(x == num)
                return true;
        }
        return false;
    }

    private static int getIndex(int[] nums, int num){
        for(int idx = 0; idx < nums.length; idx++){
            if(nums[idx] == num)
                return idx;
        }
        return -1;
    }

    private static int maxInArray(int[] nums){
        int biggest;
        if(nums.length == 0)
            return -1;
        else
            biggest = nums[0];

        for(int x : nums){
            if(biggest < x)
                biggest = x;
        }

        return biggest;
    }

    private static String[] reverseTableOfStrings(String[] nums){
        int length = nums.length;
        String[] res = new String[length];

        for(int i = 0; i < length; i++){
            res[length-i-1] = nums[i];
        }

        return res;
    }

    private static void printHalfTable(int[] nums){
        for(int i = 0; i < nums.length; i++){
            if(i % 2 != 0)
                System.out.println(nums[i]);
        }

    }

    private static int[] sumOfTables(int[] nums1, int[] nums2){
        int[] EMPTY_ARRAY = {};

        if(nums1.length != nums2.length)
            return EMPTY_ARRAY;
        else{
            int[] res = new int[nums1.length];

            for(int idx = 0; idx < nums1.length; idx++)
                res[idx] = nums1[idx] + nums2[idx];

            return res;
        }
    }

    private static boolean sameArrays(int[] nums1, int[] nums2){
        if(nums1.length != nums2.length)
            return false;

        for(int idx = 0; idx < nums1.length; idx++){
            if(nums1[idx] != nums2[idx])
                return false;
        }

        return true;
    }

    private static boolean isPrime(int num){
        boolean res = false;

        if(num < 1)
            return res;

        for (int i = 2; i < num; i++)
            if (num % i == 0)
                return false;

        return true;
    }

    private static void arrayDuplicates(int[] nums){
        int prev = 0;
        ArrayList<Integer> list = new ArrayList<>();

        for(int num : nums)
            list.add(num);

        Collections.sort(list);

        for (int num : list){
            if (list.indexOf(num) != 0) {
                if (prev == num)
                    System.out.println(num);
            }
            prev = num;
        }
    }

    //set number 3

    private static void write20(){
        for (int i = 1; i <= 20 ; i++) {
            System.out.print(i + ", ");
        }

        System.out.println("");

        int j = 1;
        while(j <= 20){
            System.out.print(j + ", ");
            j++;
        }

        System.out.println("");
    }

    private static void writeOdds(){
        int c = 20;
        for(int i = 1; i <= c; i++){
            if(i % 2 == 1)
                System.out.print(i + ", ");
            else
                c++;
        }

        System.out.println("");

        int j = 1;
        c = 20;
        while(j <= c){
            if(j % 2 == 1)
                System.out.print(j + ", ");
            else
                c++;
            j++;
        }

        System.out.println("");
    }

    private static void write20v2(){
        int c = 20+15;
        int j = 0;
        for (int i = 15; i <= c ; i+=3) {

            System.out.print(i-15-j*2 + ", ");
            j++;
            c += 2;
        }

        System.out.println("");
    }

    private static void printArray(int n){
        int[][] arr = new int[n][n];
        for(int i = 0 ; i < n; i++){
            for(int j = 0; j < n; j++){
                arr[i][j]=3;
                System.out.print("3");
            }
            System.out.println("");
        }
    }

    private static void printArrayv2(int n){
        int[][] arr = new int[n][n];
        for(int i = 0 ; i < n; i++){
            for(int j = 0; j < n; j++){
                if(i == j){
                    arr[i][j]=1;
                    System.out.print("1");
                }else{
                    arr[i][j]=0;
                    System.out.print("0");
                }
            }
            System.out.println("");
        }
    }

    private static void printArrayv3(int n){
        int[][] arr = new int[n][n];
        for(int i = 0 ; i < n; i++){
            for(int j = 0; j < n; j++){
                if((n - 1 - i) == j){
                    arr[i][j]=1;
                    System.out.print("1");
                }else{
                    arr[i][j]=0;
                    System.out.print("0");
                }
            }
            System.out.println("");
        }
    }

    private static void printArrayv4(int n){
        int[][] arr = new int[n][n];
        for(int i = 0 ; i < n; i++){
            for(int j = 0; j < n; j++){
                if((i + 1) == j){
                    arr[i][j]=1;
                    System.out.print("1");
                }else{
                    arr[i][j]=0;
                    System.out.print("0");
                }
            }
            System.out.println("");
        }
    }

    private static int equation(int n){
        int res = 0;
        for(int i = 1; i < n + 1; i++){
            res += i*i;
        }
        return res;
    }

    private static String reverseString(String s){
        StringBuilder rev = new StringBuilder();
        int l = s.length();
        char[] arr = s.toCharArray();
        for(int i = 0; i < l; i++)
        {
            rev.append(arr[l-i-1]);
        }
        return rev.toString();
    }

    private static void showArray(int[] nums){
        System.out.println(Arrays.toString(nums));
    }

    private static void writeFromTheEnd(int num){
        String s = String.valueOf(num);
        char[] digits = s.toCharArray();
        int l = digits.length;
        for(int i = 0; i < l; i++){
            System.out.println(digits[l-i-1]);
        }
    }

    private static boolean isPolyndrom(String s){
        char[] chars = s.replaceAll("\\s", "").toLowerCase().toCharArray();
        int l = chars.length;
        for(int i = 0; i < l/2; i++){
            if(chars[i] != chars[l-i-1])
                return false;
        }

        return true;
    }

    //Set 4

    private static void set4Task1(){
//        Stwórz tablicę 10 elementową:
//        1. Wypełnioną kolejnymi liczbami: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
//        2. Co druga liczba zwiększona o poprzednika
//        3. Losowe wartości: : (rand.nextInt(to-from) + from)
        int[] arr = {0,1,2,3,4,5,6,7,8,9};
        int[] arr2 = {0,1,2,3,4,5,6,7,8,9};
        int[] arr3 = new int[10];
        Random rnadomizer = new Random();

        for(int i = 1; i < arr.length; i++){
            arr2[i]+= arr2[i-1];
            arr3[i] = rnadomizer.nextInt(10);
            i++;
        }
//        Następnie wyświetl:
//        1. Wszystkie od końca
//        2. Wszystkie nieparzyste
//        3. Sumę wszystkich poprzednich wartości dla każdego elementu
//        4. Sumę wszystkich większych od 5.

//        1.
        showArrayFromTheEnd(arr);
        showArrayFromTheEnd(arr2);
        showArrayFromTheEnd(arr3);
//        2.
        showArrayOdds(arr);
        showArrayOdds(arr2);
        showArrayOdds(arr3);
//        3
        showArrayPreviousSum(arr);
        showArrayPreviousSum(arr2);
        showArrayPreviousSum(arr3);
//        4
        showArraySumMoreThan5(arr);
        showArraySumMoreThan5(arr2);
        showArraySumMoreThan5(arr3);

    }

    private static void showArrayFromTheEnd(int[] arr){
        for (int i = 0; i < arr.length; i++){
            System.out.print(arr[arr.length - i - 1] + ",");
        }
        System.out.println("");
    }

    private static void showArrayOdds(int[] arr){
        for (int i = 0; i < arr.length; i++){
            int a = arr[i];
            if(a % 2 != 0)
                System.out.print(a + ",");
        }
        System.out.println("");
    }

    private static void showArrayPreviousSum(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++){
            sum += arr[i];
            System.out.print(sum + ",");
        }
        System.out.println("");
    }

    private static void showArraySumMoreThan5(int[] arr){
        int sum = 0;
        for (int i = 0; i < arr.length; i++){
            int a = arr[i];
            if(a > 5)
                sum += a;
        }
        System.out.println(sum);
    }

    private static void set4Task2(){
        draw1();
        draw2();
        draw3();
        draw4();
    }

    private static void draw1(){
        for(int i = 0; i < 6; i++){
            for(int j= 0; j < 6; j++){
                if(i >= j)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println("");
        }
    }

    private static void draw2(){
        for(int i = 0; i < 6; i++){
            for(int j= 0; j < 6; j++){
                if(i == j)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println("");
        }
    }

    private static void draw3() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if (i == 0 || i == 5)
                    System.out.print("*");
                else
                if(j == 0 || j == 5)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println("");
        }
    }

    private static void draw4() {
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if( j == i || j == 6 - i - 1)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println("");
        }
    }

    private static void set4Task3(){
        int[] arr = {1,2,3,4,5,6,7,8,9};
        System.out.println("set4Task31(arr): " + Arrays.toString(set4Task31(arr)));
        String[] arr2 = {"Damian", "Krzysztof", "Jan", "Marzena", "Paulina", "Jolanta", "Wiesław","Kazimierz", "Aleksandra", "Marta"};
        System.out.println("set4Task31(arr): " + Arrays.toString(set4Task32(arr2)));
        set4Task33(arr);
    }

    private static int[] set4Task31(int[] arr){
//        Napisz metodę która przyjmuje tablicę liczb i zwraca tą samą tablicę
//        ale zamiast każdej liczby jest 0 jeśli liczba na danym miejscu jest
//        parzysta i 1 w przeciwnym wypadku.

        int l = arr.length;
        int[] res = new int[l];
        for (int i = 0; i < l; i++){
            res[i] = arr[i] % 2 == 0 ? 0 : 1;
        }
        return res;
    }

    private static String[] set4Task32(String[] arr){
//        Napisz metodę która przyjmuje tablicę wypełnioną dowolną ilością
//        imion polskich a następnie zwróci nową tablicę złożoną tylko z tych
//        imion podanej tablicy które są męskie.

        ArrayList<String> maleNamesList = new ArrayList<String>();
        for(String name : arr){
            if(!name.toLowerCase().endsWith("a"))
                maleNamesList.add(name);
        }

        return maleNamesList.toArray(new String[0]);
    }

    private static void set4Task33(int[] arr){
//        Napisz metodę, która przyjmie tablicę liczb i dla każdego elementu tej
//        tablicy wypiszę: „n jest liczbą podzielną przez m” gdzie n to liczba z
//        tablicy natomiast m wynosi 2 lub 3 lub 5 lub 7 w zależności jaki jest
//        największy dzielnik liczby n

        for(int number : arr){
            if(number % 7 == 0)
                System.out.println(number  + " jest liczbą podzielną przez 7.");
            else if(number % 5 == 0)
                System.out.println(number  + " jest liczbą podzielną przez 5.");
            else if(number % 3 == 0)
                System.out.println(number  + " jest liczbą podzielną przez 3.");
            else if(number % 2 == 0)
                System.out.println(number  + " jest liczbą podzielną przez 2.");
        }
    }

    private static void set4Task4(){
        Car car = new Car();
        Bike bike = new Bike();
        car.drive(120);
        bike.drive(200);
        System.out.println("Car velocity is: " + car.getVelocity());
        System.out.println("Bike velocity is: " + bike.getVelocity());
    }

    private static void set4Task5(){
        MyNumber a = new MyNumber(5);
        System.out.println("a=5");
        System.out.println("Is a odd? " + a.isOdd());
        System.out.println("Is a even? " + a.isEven());
        System.out.println("a^3=" + a.pow(new MyNumber(3)));
        System.out.println("a-3=" + a.substract(new MyNumber(3)).toString());
        System.out.println("a+3=" + a.add(new MyNumber(3)).toString());
    }
}
