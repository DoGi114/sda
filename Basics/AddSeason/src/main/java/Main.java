import java.util.ArrayList;
import java.util.List;

public class Main {
    private static List<Integer> seasons;

    public static void main(String[] args) {

    }

    public static void initializeSeasonsList(){
        // 11 - winter
        // 12 - spring
        // 21 - summer
        // 22 - autumn
        seasons = new ArrayList<>();
        seasons.add(11);
        seasons.add(12);
        seasons.add(21);
        seasons.add(22);
    }

    public static String addSeason(String input, int number){
        int season = Integer.parseInt(input.substring(4));
        int year = Integer.parseInt(input.substring(0,4));
        StringBuilder sb = new StringBuilder();

        int idx = seasons.indexOf(season) ;
        int newIdx = (idx + number - 1) % 4;
        int newYear = year;

        for(int i = idx; i <= (number + idx); i++){
            if( i % 4 == 0 && i != idx){
                newYear++;
            }
        }

        sb.append(newYear);
        sb.append(seasons.get(newIdx));

        return sb.toString();
    }
}
