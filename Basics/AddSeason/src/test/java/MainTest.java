import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {

    @Test
    public void doAddSeasonTest(){
        Main.initializeSeasonsList();
        assertEquals(Main.addSeason("201911", 1), "201911");
        assertEquals(Main.addSeason("202012", 2), "202021");
        assertEquals(Main.addSeason("201821", 3), "201911");
    }
}
