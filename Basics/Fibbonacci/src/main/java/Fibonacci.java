import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Fibonacci {

    public static List<String> GenerateFibonacciSeries(int digits){
        List<String> res = new ArrayList<>();
        for (int i = 0 ; i <= digits ; i++){
            res.add(Generate(new BigInteger(String.valueOf(i))).toString());
        }
        return res;
    }


    public static BigInteger Generate(BigInteger digit) {
        if (digit.compareTo(new BigInteger("1")) <= 0 ) {
            return digit;
        } else {
            BigInteger newFibo = Generate(digit.subtract(BigInteger.ONE)).add(Generate(digit.subtract(new BigInteger("2"))));
            return newFibo;
        }
    }

    public static int GenerateInt(int n){
        if(n <= 1) return n;
        return GenerateInt(n-2)+GenerateInt(n-1);
    }
}
