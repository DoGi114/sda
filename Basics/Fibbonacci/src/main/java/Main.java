import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {

        System.out.println(Fibonacci.Generate(new BigInteger("30")));
        System.out.println(Fibonacci.Generate(new BigInteger("30")));
        System.out.println(String.join(",", Fibonacci.GenerateFibonacciSeries(30)));
    }
}
