public class ShortSMS {
    public String shortSms(String text) {
        StringBuilder res = new StringBuilder();
        String[] words = text.split(" ");
        for (String word : words) {
            res.append(capitalize(word));
        }
        return res.toString();
    }

    private String capitalize(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public int getNumberOfMessages(String text) {
        //1SMS = 160 chars;
        return text.length() / 160 + 1;
    }

}

