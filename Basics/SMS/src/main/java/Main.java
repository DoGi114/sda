import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ShortSMS shortSMS = new ShortSMS();
        System.out.println("Write your message:");
        String shortMessage = shortSMS.shortSms(scanner.nextLine());
        System.out.println(shortMessage);
        System.out.println("You need to pay for " + shortSMS.getNumberOfMessages(shortMessage) + " text messages.");
    }



}
