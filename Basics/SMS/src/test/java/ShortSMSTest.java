import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShortSMSTest {

    @Test
    void shortSMSTest() {
        ShortSMS shortSMS = new ShortSMS();
        assertEquals(shortSMS.shortSms("Ala ma kota, kot ma ale."), "AlaMaKota,KotMaAle.");
    }

    @Test
    void numberOfMessages() {
        ShortSMS shortSMS = new ShortSMS();
        assertEquals(shortSMS.getNumberOfMessages("Ala ma kota, kot ma ale."), 1);
        assertEquals(shortSMS.getNumberOfMessages("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris cursus dictum dolor, sit amet vestibulum arcu placerat eu. Etiam sapien leo, cursus nec neque sit amet, scelerisque vehicula dui. Vivamus ullamcorper blandit pulvinar. Nam vitae iaculis justo. Donec rutrum, metus convallis molestie mollis, est urna varius dui, id rhoncus odio risus ut metus. Sed molestie risus lectus, sit amet blandit urna vulputate ac. Aliquam commodo rhoncus scelerisque."), 3);
    }
}
