public class CatsView {
    private CatsController controller ;

    public CatsView(){
        controller = new CatsController();
    }

    public void showURL(){
        System.out.println("URL: " + controller.showCat().getURL());
    }
}
