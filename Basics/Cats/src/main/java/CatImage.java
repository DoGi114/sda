public class CatImage {
    private String URL;
    private String path;
    private String resolution;
    private String size;

    public CatImage(final String URL, final String path, final String resolution, final String size) {
        this.URL = URL;
        this.path = path;
        this.resolution = resolution;
        this.size = size;
    }

    public CatImage(final String URL) {
        this.URL = URL;
    }

    public String getURL() {
        return this.URL;
    }

    public String getResolution() {
        return this.resolution;
    }

    public String getSize() {
        return this.size;
    }

    public void setResolution(final String resolution) {
        this.resolution = resolution;
    }

    public void setSize(final String size) {
        this.size = size;
    }

    public String getPath() {
        return this.path;
    }

    public void setPath(final String path) {
        this.path = path;
    }
}
