import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

public class CatsController {

    private CatsDAO dao;

    public CatsController(){
        try {
            dao = new CatsDAO();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public CatImage showCat(){
        CatImage cat = new CatImage(dao.getURLtoImage());
        Image image = null;
        try {
            URL url = new URL(cat.getURL());
            image = ImageIO.read(url);
            cat.setResolution(image.getWidth(null) + "x" + image.getHeight(null));
            cat.setPath(saveImage(cat.getURL()));
            cat.setSize(getImageSize(cat.getPath()));
            //TODO:Blur nie działa..... -.-
            blurImage(cat.getPath());
            postToSlack(cat.getURL());

        } catch (IOException e) {

        }

        return Optional.of(cat).get();
    }

    private String getImageSize(String path){
        File file = new File(path);
        return file.length() + "b";
    }

    private String saveImage(String sUrl){
        URL url = null;
        try {
            url = new URL(sUrl);
            String fileName = url.getFile();
            String destName = "./Downloads" + fileName.substring(fileName.lastIndexOf("/"));

            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream(destName);

            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                os.write(b, 0, length);
            }

            is.close();
            os.close();

            return new File(destName).getCanonicalPath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
            return "";
    }

    private void saveImage(BufferedImage bi, String path ){
        URL url = null;
        try {
            url = new File(path).toURI().toURL();
            String destName = path.replace(".", "_blured.");

            InputStream is = url.openStream();
            OutputStream os = new FileOutputStream(destName);

            byte[] b = new byte[2048];
            int length;

            while ((length = is.read(b)) != -1) {
                os.write(b, 0, length);
            }

            is.close();
            os.close();

            //return new File(destName).getAbsolutePath();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //return "";
    }

    private void blurImage(String path){
        BufferedImage myImage = null;
        try {
            myImage = ImageIO.read(new File(path));

        BufferedImage filteredImage = new BufferedImage(myImage.getWidth(null), myImage
                .getHeight(null), BufferedImage.TYPE_BYTE_GRAY);

        Graphics g1 = filteredImage.getGraphics();
        g1.drawImage(myImage, 400, 200, null);

        float[] blurKernel = { 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f, 1 / 9f };

        BufferedImageOp blur = new ConvolveOp(new Kernel(3, 3, blurKernel));
        saveImage(blur.filter(myImage, null), new File(path).getAbsolutePath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void postToSlack(String msg){
        JSONObject json = new JSONObject();
        //TODO: username nie dziala o.O
        json.put("text", msg);
        json.put("icon_url", "https://avatarfiles.alphacoders.com/486/48632.jpg");
        json.put("username", "Cat images");
        StringEntity entity = new StringEntity(json.toString(),
                ContentType.APPLICATION_FORM_URLENCODED);

        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpPost request = new HttpPost("https://hooks.slack.com/services/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");
        request.setEntity(entity);

        HttpResponse response = null;
        try {
            response = httpClient.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
