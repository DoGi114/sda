package classes;

import classes.datastorages.Cart;
import classes.datastorages.Store;
import classes.views.ConsoleCartView;
import classes.views.ConsoleStoreView;
import classes.views.SwingStoreView;

public class Main {
    public static void main(String[] args) {
        Store store = new Store();
        SwingStoreView storeView = new SwingStoreView(store);
        storeView.showProductList();
//        ConsoleStoreView consoleStoreView = new ConsoleStoreView(store);
//        consoleStoreView.showProductList();
//        Cart cart = new Cart();
//        cart.addToCart(store.getAllProducts().get(2));
//        cart.addToCart(store.getAllProducts().get(2));
//        cart.addToCart(store.getAllProducts().get(2));
//        cart.addToCart(store.getAllProducts().get(3));
//        ConsoleCartView consoleCartView = new ConsoleCartView(cart);
//        consoleCartView.showCart();
//        consoleCartView.checkOut();
//        consoleStoreView.showProductList();
    }
}
