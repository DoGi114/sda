package classes.interfaces;

public interface ICartView {
    void showCart();
    void checkOut();
}
