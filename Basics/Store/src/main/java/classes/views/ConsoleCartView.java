package classes.views;

import classes.datastorages.Cart;
import classes.interfaces.ICartView;
import classes.models.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ConsoleCartView implements ICartView {
    private final Cart cart;
    private final List<Product> alreadyInCart;

    private final int NAME_SPACE = 22;
    private final int AMOUNT_SPACES = 34;
    private final int PRICE_SPACES = 48;

    private final String NAME_HEADER = "Name";
    private final String AMOUNT_HEADER = "Amount";
    private final String PRICE_HEADER = "Price";

    public ConsoleCartView(Cart cart) {
        this.cart = cart;
        this.alreadyInCart = new ArrayList<>();
    }

    public void showCart() {
        printCartHeader();
        for (Product product : cart.getAllProductsInCart()) {
            if (!alreadyInCart.contains(product))
                printCartRow(product);
            alreadyInCart.add(product);
        }
        printCartFooter();
    }

    private void printCartHeader() {
        System.out.println("-------------------------------------------------");
        StringBuilder sb = new StringBuilder();
        sb.append("| ");
        sb.append(NAME_HEADER);
        while (sb.toString().length() < NAME_SPACE) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(AMOUNT_HEADER);
        while (sb.toString().length() < AMOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(PRICE_HEADER);
        while (sb.toString().length() < PRICE_SPACES) {
            sb.append(" ");
        }
        sb.append("|");
        System.out.println(sb);
        System.out.println("-------------------------------------------------");
    }

    private void printCartFooter() {
        System.out.println("-------------------------------------------------");
        System.out.println("SUM: " + cart.getTotalPrice());
    }

    private void printCartRow(Product product) {

        StringBuilder sb = new StringBuilder();
        sb.append("| ");
        sb.append(product.getName());
        while (sb.toString().length() < NAME_SPACE) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(cart.getAmountOfProductInCart(product));
        while (sb.toString().length() < AMOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(product.getPrice());
        while (sb.toString().length() < PRICE_SPACES) {
            sb.append(" ");
        }
        sb.append("|");
        System.out.println(sb);
    }

    public void checkOut() {
        System.out.println("Do you want to buy those items? YES/NO");
        Scanner scanner = new Scanner(System.in);
        String response = scanner.nextLine();
        if (response.toUpperCase().equals("YES")) {
            cart.checkOut();
        } else if (response.toUpperCase().equals("NO")) {
            System.out.println("Ok, continue your shoping.");
        } else {
            System.err.println("Bad input, repeat.");
            checkOut();
        }
    }
}
