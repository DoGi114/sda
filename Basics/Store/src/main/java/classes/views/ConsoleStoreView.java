package classes.views;

import classes.datastorages.Store;
import classes.interfaces.IStoreView;
import classes.models.Product;

public class ConsoleStoreView implements IStoreView {
    private Store store;

    private final int NAME_SPACES = 22;
    private final int AMOUNT_SPACES = 40;
    private final int DISCOUNT_SPACES = 53;
    private final int TAX_SPACES = 62;
    private final int PRICE_SPACES = 73;

    private final String NAME_HEADER = "Name";
    private final String AMOUNT_HEADER = "Amount";
    private final String DISCOUNT_HEADER = "Discount";
    private final String TAX_HEADER = "Tax";
    private final String PRICE_HEADER = "Price";

    public ConsoleStoreView(Store store) {
        this.store = store;
    }

    public void showProductList() {
        printProductListHeader();
        for (Product product : store.getAllProducts()) {
            printProductListRow(product);
        }
        printProductListFooter();

    }

    private void printProductListHeader() {
        System.out.println("--------------------------------------------------------------------------");
        StringBuilder sb = new StringBuilder();
        sb.append("| ");
        sb.append(NAME_HEADER);
        while (sb.toString().length() < NAME_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(AMOUNT_HEADER);
        while (sb.toString().length() < AMOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(DISCOUNT_HEADER);
        while (sb.toString().length() < DISCOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(TAX_HEADER);
        while (sb.toString().length() < TAX_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(PRICE_HEADER);
        while (sb.toString().length() < PRICE_SPACES) {
            sb.append(" ");
        }
        sb.append("|");
        System.out.println(sb);
        System.out.println("--------------------------------------------------------------------------");
    }

    private void printProductListFooter() {
        System.out.println("--------------------------------------------------------------------------");
    }

    private void printProductListRow(Product product) {
        StringBuilder sb = new StringBuilder();
        sb.append("| ");
        sb.append(product.getName());
        while (sb.toString().length() < NAME_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(product.getAmount());
        while (sb.toString().length() < AMOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(product.getDiscountString());
        while (sb.toString().length() < DISCOUNT_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(product.getTaxString());
        while (sb.toString().length() < TAX_SPACES) {
            sb.append(" ");
        }
        sb.append("| ");
        sb.append(product.getPrice());
        while (sb.toString().length() < PRICE_SPACES) {
            sb.append(" ");
        }
        sb.append("|");
        System.out.println(sb);
    }
}
