package classes.views;

import classes.datastorages.Store;
import classes.interfaces.IStoreView;
import classes.models.Product;
import classes.swing.SwingStore;

public class SwingStoreView implements IStoreView {

    SwingStore swingStore ;
    Store store;

    public SwingStoreView(Store store){
        this.store = store;
        this.swingStore = new SwingStore(store);
    }

    @Override
    public void showProductList() {
        for (Product product : store.getAllProducts()) {
            swingStore.addListRow(product);
        }
    }
}
