package classes.generators;

import java.util.ArrayList;
import java.util.List;

public class IdGenerator {

    private static final List<Integer> idList = new ArrayList<>();

    public static int generate() {
        int res = 0;
        for (int i = 0; i < idList.size(); i++) {
            if (i != idList.get(i)) {
                res = i;
            }
        }

        return res;
    }
}
