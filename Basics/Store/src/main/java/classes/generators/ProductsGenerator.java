package classes.generators;

import classes.models.Product;
import enums.RandomStringGeneratorMode;

import java.util.List;
import java.util.Random;

public class ProductsGenerator {

    public static void generate(List<Product> productList, int number) {
        Random rnd = new Random();

        for (int i = 0; i < number; i++) {
            Product product = new Product(RandomStringGenerator.generateRandomString(10, RandomStringGeneratorMode.ProductName), rnd.nextInt(50), rnd.nextInt(100), "0%", "22%");
            productList.add(product);
        }
    }
}
