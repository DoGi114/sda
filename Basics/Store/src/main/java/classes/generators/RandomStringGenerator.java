package classes.generators;

import enums.RandomStringGeneratorMode;

import java.util.Random;

public class RandomStringGenerator {
    private static final String CHAR_LOWER = "abcdefghijklmnopqrstuvwxyz";
    private static final String CHAR_UPPER = CHAR_LOWER.toUpperCase();
    private static final String NUMBER = "0123456789";
    private static final String ALL_CHARS = CHAR_LOWER + CHAR_UPPER + NUMBER;

    public static String generateRandomString(int length, RandomStringGeneratorMode mode) {
        Random random = new Random();

        if (length < 1) throw new IllegalArgumentException();

        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int rndCharAt;
            char rndChar;

            // 0-62 (exclusive), random returns 0-61

            switch (mode) {
                case Name:
                case Surname:
                case Username:
                case ProductName:
                    if (i == 0) {
                        rndCharAt = random.nextInt(CHAR_UPPER.length());
                        rndChar = CHAR_UPPER.charAt(rndCharAt);
                    } else {
                        rndCharAt = random.nextInt(CHAR_LOWER.length());
                        rndChar = CHAR_LOWER.charAt(rndCharAt);
                    }
                    break;
                case Password:
                default:
                    rndCharAt = random.nextInt(ALL_CHARS.length());
                    rndChar = ALL_CHARS.charAt(rndCharAt);
                    break;
            }
            sb.append(rndChar);
        }

        return sb.toString();

    }
}
