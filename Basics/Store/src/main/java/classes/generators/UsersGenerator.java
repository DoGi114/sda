package classes.generators;

import classes.models.User;
import enums.RandomStringGeneratorMode;

import java.util.List;
import java.util.Random;

public class UsersGenerator {

    public static void generate(List<User> userList, int number) {
        Random rnd = new Random();

        for (int i = 0; i < number; i++) {
            User user = new User(RandomStringGenerator.generateRandomString(10, RandomStringGeneratorMode.Name),
                    RandomStringGenerator.generateRandomString(10, RandomStringGeneratorMode.Surname),
                    RandomStringGenerator.generateRandomString(10, RandomStringGeneratorMode.Username),
                    RandomStringGenerator.generateRandomString(20, RandomStringGeneratorMode.Password));
            userList.add(user);
        }
    }
}
