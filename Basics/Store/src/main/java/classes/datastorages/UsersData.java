package classes.datastorages;

import classes.generators.UsersGenerator;
import classes.models.User;

import java.util.ArrayList;
import java.util.List;

public class UsersData {
    List<User> userList;

    public UsersData() {
        userList = new ArrayList<>();
        UsersGenerator.generate(userList, 10);
    }

    public void addUser(User user) {
        try {
            if (user == null) {
                throw new Exception("Removed product cant be null.");
            }
            userList.add(user);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public List<User> getAllUsers() {
        return userList;
    }

    public void removeUser(User user) {
        try {
            if (user == null) {
                throw new Exception("Removed product cant be null.");
            }
            userList.remove(user);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public User getUserByLogin(String login) {
        User res = null;
        for (User user : userList) {
            if (user.getUsername().equals(login)) {
                res = user;
            }
        }

        return res;
    }

    public User getUserById(int id) {
        User res = null;
        for (User user : userList) {
            if (user.getId() == id) {
                res = user;
            }
        }

        return res;
    }
}
