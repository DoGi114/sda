package classes.datastorages;

import classes.models.Product;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Product> productList;

    public Cart() {
        productList = new ArrayList<>();
    }

    public void addToCart(Product product) {
        try {
            if (product == null) {
                throw new Exception("Added product cant be null.");
            }
            productList.add(product);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public void removeFromCart(Product product) {

        try {
            if (product == null) {
                throw new Exception("Added product cant be null.");
            }
            productList.remove(product);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public List<Product> getAllProductsInCart() {
        return productList;
    }

    public double getTotalPrice() {
        int price = 0;
        for (Product product : productList) {
            price += product.getPrice() - product.getPrice() * product.getDiscount();
        }
        return price;
    }

    public int getAmountOfProductInCart(Product product) {
        int res = 0;
        for (Product product1 : productList) {
            if (product.equals(product1)) {
                res++;
            }
        }
        return res;
    }

    public void checkOut() {
        List<Product> alreadyCheckedout = new ArrayList<>();
        for (Product product : productList) {
            if (!alreadyCheckedout.contains(product)) {
                product.setAmount(product.getAmount() - getAmountOfProductInCart(product));
            }
            alreadyCheckedout.add(product);
        }

        productList.clear();
    }
}
