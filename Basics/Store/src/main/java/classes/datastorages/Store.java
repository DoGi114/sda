package classes.datastorages;

import classes.generators.ProductsGenerator;
import classes.models.Product;

import java.util.ArrayList;
import java.util.List;

public class Store {
    private List<Product> productList;

    public Store() {
        productList = new ArrayList<>();
        ProductsGenerator.generate(productList, 10);
    }

    public void addProduct(Product product) {
        try {
            if (product == null) {
                throw new Exception("Added product cant be null.");
            }
            productList.add(product);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public List<Product> getAllProducts() {
        return productList;
    }

    public void removeProduct(Product product) {
        try {
            if (product == null) {
                throw new Exception("Removed product cant be null.");
            }
            productList.remove(product);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    public int getProductAmount(Product product) {
        return productList.get(productList.indexOf(product)).getAmount();
    }
}
