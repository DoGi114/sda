package classes.models;

public class Product {
    private String name;
    private int amount;
    private double price;
    private boolean isOnSale;
    private double discount;
    private double tax;

    public Product() {
    }

    public Product(String name, int amount, double price, String discountPercentage, String taxPercentage) {
        this.name = name;
        this.amount = amount;
        this.price = price;
        this.discount = changeStringToPercentage(discountPercentage);
        this.tax = changeStringToPercentage(taxPercentage);

        if (discount > 0) {
            isOnSale = true;
        }
    }

    private double changeStringToPercentage(String percentageString) {
        int percentageNumber = 0;
        try {
            if (!percentageString.endsWith("%")) {
                throw new Exception("Discount have to end with \"%\".");
            }

            if ((percentageString.length() < 2) || (percentageString.length() > 3)) {
                throw new Exception("Discount cant be bigger than 99% or smaller than 1%.");
            }

            if (percentageString.contains(".") || percentageString.contains(",")) {
                throw new Exception("Discount cant have decimal places.");
            }

            percentageNumber = Integer.parseInt(percentageString.substring(0, percentageString.length() - 1));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return ((double) percentageNumber) / 100;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return this.amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return this.price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isOnSale() {
        return this.isOnSale;
    }

    public void setOnSale(boolean onSale) {
        this.isOnSale = onSale;
    }

    public double getDiscount() {
        return this.discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
        if (this.discount > 0) {
            this.isOnSale = true;
        }
    }

    public void setDiscount(String discountPercentage) {
        this.discount = changeStringToPercentage(discountPercentage);
        if (this.discount > 0) {
            this.isOnSale = true;
        }
    }

    public String getDiscountString() {
        return (int) (this.discount * 100) + "%";
    }

    public double getTax() {
        return this.tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

    public void setTax(String taxPercentage) {
        this.tax = changeStringToPercentage(taxPercentage);
    }

    public String getTaxString() {
        return (int) (this.tax * 100) + "%";
    }
}
