package enums;

public enum RandomStringGeneratorMode {
    Name, Surname, Username, Password, ProductName
}
