import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main(String[] args) {
        showClassInfo(Car.class);
        searchForAnnotationInClass(Car.class);
    }

    private static void showClassInfo(Class c){
        System.out.println("Class name: " + c.getName());
        System.out.println("Class constructors number: " + c.getConstructors().length);
        System.out.println("Class methods number: " + c.getMethods().length);
        for(Method m : c.getMethods()){
            System.out.println("Method " + m.getName() + " has " + m.getParameterCount() + " parameters.");
        }
        for(Field f : c.getFields()){
            System.out.println("field " + f.getName() + " is type of " + f.getType());
        }
    }

    private static void searchForAnnotationInClass(Class c){
        for(Method m : c.getMethods()){
            Annotation a = m.getAnnotation(SuperMethod.class);
            if(a != null){
                System.out.println("Method with our reflection called " + a + " is " + m.getName());
                try {
                    Object o = c.getConstructor(String.class, String.class, String.class).newInstance("Car", "M3", "BMW");
                    if(o instanceof Car){
                        Car car = (Car) o;
                        m.invoke(car, 70);
                        System.out.println("Our " + car.getBrand() + " " + car.getModel() + " " + (car.isMoving() ? "is moving" : "is not moving"));
                    }
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
