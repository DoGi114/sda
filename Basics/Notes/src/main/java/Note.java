import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

public class Note {
    private int id;
    private List<String> tags;
    private String memo;
    private LocalDateTime timestamp;

    public Note(String memo, List<String> tags){
        this.memo = memo;
        this.tags = tags;
        this.timestamp = LocalDateTime.now();
        this.id = IdGenerator.generateId();
    }

    //Not in task, my own idea
    public Note(String memo){
        this.memo = memo;
        this.tags = generateTags();
        this.timestamp = LocalDateTime.now();
        this.id = IdGenerator.generateId();
    }

    //Not in task, my own idea
    private List<String> generateTags() {
        List<String> res;
        String[] words = memo.split("[ ,.]");

        res = Arrays.asList(words);

        return res;
    }

    public boolean match(String s){
        String[] memoWords = memo.split("[ .,]");

        for(String tag : tags){
            if (tag.toLowerCase().equals(s.toLowerCase())) {
                return true;
            }
        }

        for(String memoWord : memoWords){
            if(memoWord.toLowerCase().equals(s.toLowerCase())){
                return true;
            }
        }

        return false;
    }

    public int getId(){
        return id;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    //Not in task, my own idea
    public String getPrettyTimestamp(){
        // Custom format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return timestamp.format(formatter);
    }
}
