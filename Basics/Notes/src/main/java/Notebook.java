import java.util.ArrayList;
import java.util.List;

public class Notebook {
    private  List<Note> notes;

    public Notebook(){
        notes = new ArrayList<>();
    }

    public void addNewNote(String memo, List<String> tags){
        Note note = new Note(memo, tags);
        notes.add(note);
    }

    //Not in task, my own idea
    public void addNewNote(String memo){
        Note note = new Note(memo);
        notes.add(note);
    }

    public void setMemoAt(int id, String memo){
        Note note = notes.get(id);
        note.setMemo(memo);
    }

    public void setTagsAt(int id, List<String> tags){
        Note note = notes.get(id);
        note.setTags(tags);
    }

    public List<Note> search(String searchString){
        List<Note> res = new ArrayList<>();

        for(Note note : notes){
            if(note.match(searchString)){
                res.add(note);
            }
        }

        return res;
    }

    //Not in task, my own idea
    public Note getNote(int idx){
        return notes.get(idx);
    }
}
