import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Notebook notebook = new Notebook();
        List<String> tags = new ArrayList<>();
        tags.add("lubie");
        tags.add("placki");
        notebook.addNewNote("Lubie placki", tags);
        tags = new ArrayList<>();
        tags.add("Nie");
        tags.add("lubie");
        tags.add("plackow");
        notebook.addNewNote("Nie Lubie plackow", tags);
        tags = new ArrayList<>();
        tags.add("Kocham");
        tags.add("placki");
        notebook.setMemoAt(0, "Kocham placki");
        notebook.setTagsAt(0, tags);
        notebook.addNewNote("Chwalmy Pana, bo nie mam pomyslu.", tags);

        Note note = notebook.getNote(0);
        System.out.println("Note nr." + note.getId() + " contains " + note.getMemo() + " with tags: [" + String.join(",", note.getTags()) + "] created at " + note.getPrettyTimestamp());

        note = notebook.getNote(1);
        System.out.println("Note nr." + note.getId() + " contains " + note.getMemo() + " with tags: [" + String.join(",", note.getTags()) + "] created at " + note.getPrettyTimestamp());

        note = notebook.getNote(2);
        System.out.println("Note nr." + note.getId() + " contains " + note.getMemo() + " with tags: [" + String.join(",", note.getTags()) + "] created at " + note.getPrettyTimestamp());
    }
}
