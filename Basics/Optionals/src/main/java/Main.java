import java.util.Optional;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Optional<Person> o1 = Optional.of(randomizePerson());
        Optional<Person> o2 = Optional.ofNullable(randomizePerson());
        Optional<Person> o3 = Optional.empty();

        //Not like this
        if(o2.isPresent()) {
            Person p1 = o2.get();
        }
        //But like this
        Person p2 = o2.orElse(new Person());
        //Or like lambda
        o2.ifPresent(p3 -> System.out.println(p3));

        for(int i = 0; i < 50; i++) {
            writeLn(randomizePerson());
        }

        Country country = new Country("Poland", "PL");
        Address address = new Address("Górnicza", 41, country);
        User user = new User("Damian", "Nguyen", 27, address);

        System.out.println(getUserCountryCode(user));


    }

    private static String getUserCountryCode(User user){
        return Optional.ofNullable(user).map(User::getAddress).map(Address::getCountry).map(Country::getCountryCode).orElse("lack of country code");
    }

    private static Person randomizePerson(){
        Random rnd = new Random();
        if(rnd.nextInt(100) % 2 == 0){
            return new Person("Damian", "Nguyen", rnd.nextInt(30));
        }else{
            return null;
        }
    }

    private static void writeLn(Person person){
        Optional<Person> optional = Optional.ofNullable(person);
        //2.
        //optional.ifPresent(person1 -> System.out.println(person1.getName()));
        //3.
        optional.filter(person1 -> person1.getAge()>18)
                .ifPresent(person1 -> System.out.println(person1.getName() + " age : " + person1.getAge()));
    }
}
