public class BigNumber {
    private int[] digits;

    public BigNumber(String number) {
        digits = new int[number.length()];
        boolean negative = false;

        for (int i = 0; i < number.length(); i++) {
            if (i == 0 && String.valueOf(number.charAt(i)).equals("-")) {
                negative = true;
            } else {
                if (negative) {
                    digits[i] = Integer.parseInt("-" + number.charAt(i));
                    negative = false;
                } else {
                    digits[i] = Integer.parseInt(String.valueOf(number.charAt(i)));
                }
            }
        }
    }

    public BigNumber add(BigNumber b) {
        StringBuilder res = new StringBuilder();
        int nextNumber = 0;
        boolean firstBigger = false;

        int biggerLength = Math.max(getLength(), b.getLength());
        int[][] tables = makeDigitsCompatible(this, b, biggerLength);

        digits = tables[0];
        int[] digitsB = tables[1];

        int[] resArr = new int[biggerLength + 1];
        int resDigits = 0;

        for (int i = 0; i < resArr.length; i++) {
            int number;
            if (firstBigger && i == 1) {
                resArr[1] = 0;
                nextNumber = digits[1] + digitsB[i];
                resDigits++;
                continue;
            }
            if (i < biggerLength) {
                number = digits[i] + digitsB[i] + nextNumber;
                if (number <= 9) {
                    resArr[i] = number;
                } else {
                    nextNumber = number % 10;
                    firstBigger = nextNumber == 0 && i == 0;
                    if (i == 0 && firstBigger) {
                        number = 11;
                        nextNumber = 0;
                    }
                    resArr[i] = number - 10;
                }
                resDigits++;
            } else if (biggerLength + 1 == i && nextNumber != 0) {
                resArr[i] = nextNumber;
                resDigits++;
                nextNumber = 0;
            } else if (firstBigger) {
                resArr[i] = nextNumber;
                resDigits++;
                break;
            } else {
                resArr[i] = 0;
                break;
            }
        }

        for (int i = 0; i < resDigits; i++) {
            res.append(resArr[i]);
        }

        return new BigNumber(res.toString());
    }

    public BigNumber subtract(BigNumber b) {
        StringBuilder res = new StringBuilder();
        boolean lessThanZero = false;

        int biggerLength = Math.max(getLength(), b.getLength());
        int[][] tables = makeDigitsCompatible(this, b, biggerLength);

        digits = tables[0];
        int[] digitsB = tables[1];
        int[] resArr = new int[biggerLength];
        int resDigits = 0;

        for (int i = 0; i < resArr.length; i++) {
            int number;
//            digits - digitsB => digitsB - digits + change sign if number is greater
            if (digitsB.length > getLength() || (digitsB[0] > digits[0] && digitsB.length == getLength())) {
                number = digitsB[i] - digits[i];
                lessThanZero = true;
            } else {
                number = digits[i] - digitsB[i];
            }
            resArr[i] = number;
            resDigits++;
        }

        for (int i = 0; i < resArr.length; i++) {
            if (i + 1 < resArr.length) {
                if (resArr[i] > 0 && resArr[i + 1] < 0) {
                    resArr[i]--;
                    resArr[i + 1] += 10;
                }
            }
        }

        int startWith = 0;
        boolean startFound = false;

        if (lessThanZero) {
            res.append("-");
        }

        for (int i = 0; i < resDigits; i++) {
            if (!(resArr[i] == startWith && !startFound)) {
                res.append(resArr[i]);
                startFound = true;
            }
        }

        return new BigNumber(res.toString());
    }

    public BigNumber multiply(BigNumber b) {
        StringBuilder res = new StringBuilder();
        int nextNumber = 0;

        int biggerLength = Math.max(getLength(), b.getLength());
//        int[][] tables = makeDigitsCompatible(this, b, biggerLength);
        biggerLength *= biggerLength;

//        digits = tables[0];
//        int[] digitsB = tables[1];
        int[] digitsB = b.getDigits();
        int[] resArr = new int[biggerLength];
        int[] addArr = new int[digits.length];
        int resDigits = 0;

        for(int i = 0; i < digits.length; i++){
            int numberA = digits[digits.length - 1 - i];
            int number = 0;
            for(int j = 0; j < digitsB.length; j++){
                int numberB = digitsB[digitsB.length - 1 - j];
                number = numberA * numberB;

            }
            addArr[i] = number;
        }

        for(int k = 0; k < addArr.length; k++){

        }

        for (int i = 0; i < resDigits; i++) {
            if (isNegative() || b.isNegative()) {
                res.append(resArr[resArr.length - resDigits - i]);
            } else {
                res.append(resArr[resArr.length - 1 - i]);
            }
        }

        String sRes = res.reverse().toString();

        if (sRes.endsWith("-")) {
            sRes = "-" + sRes;
            sRes = sRes.substring(0, sRes.length() - 1);
        }

        return new BigNumber(sRes);
    }

    private int[][] makeDigitsCompatible(BigNumber a, BigNumber b, int length) {
        int[] digitsA = a.getDigits();
        int[] digitsB = b.getDigits();
        int[][] res = new int[2][length];
        int[] newDigits = new int[length];

        if (digitsA.length < digitsB.length) {
            int diff = length - digitsA.length;
            for (int i = 0; i < length; i++) {
                if (i < digitsA.length) {
                    if (a.isNegative() && digitsA[i] > 0) {
                        digitsA[i] = -digitsA[i];
                    }
                    newDigits[i + diff] = digitsA[i];
                } else {
                    newDigits[i - digitsA.length] = 0;
                }
            }
            res[0] = newDigits;
            res[1] = digitsB;
        } else if (digitsA.length > digitsB.length) {
            int diff = length - digitsB.length;
            for (int i = 0; i < length; i++) {
                if (i < digitsB.length) {
                    if (b.isNegative() && digitsB[i] > 0) {
                        digitsB[i] = -digitsB[i];
                    }
                    newDigits[i + diff] = digitsB[i];
                } else {
                    newDigits[i - digitsB.length] = 0;
                }
            }
            res[0] = digitsA;
            res[1] = newDigits;
        } else {
            for (int i = 0; i < length; i++) {
                if (i < digitsA.length) {
                    if (a.isNegative() && digitsA[i] > 0) {
                        digitsA[i] = -digitsA[i];
                    }
                }
            }
            res[0] = digitsA;
            for (int i = 0; i < length; i++) {
                if (i < digitsB.length) {
                    if (b.isNegative() && digitsB[i] > 0) {
                        digitsB[i] = -digitsB[i];
                    }
                }
            }
            res[1] = digitsB;
        }

        return res;
    }


    public int[] getDigits() {
        return digits;
    }

    public int getLength() {
        return digits.length;
    }

    public boolean isNegative() {
        for (int digit : digits) {
            if (digit < 0) {
                return true;
            }
        }

        return false;
    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        boolean startsWithZero = true;

        for (int digit : digits) {
            if (!(digit == 0 && startsWithZero)) {
                res.append(digit);
                startsWithZero = false;
            }
        }

        if (res.toString().equals("")) {
            res.append("0");
        }

        return res.toString();
    }
}
