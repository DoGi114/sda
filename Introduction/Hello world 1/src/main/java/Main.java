public class Main {
    public static void main(String[] args) {
        System.out.println("Damian is going to become a developer!");
        System.out.println("Yes I am!");

        myFirstMethod();
        printSurname();
        printGiveAge(27);
        printGiveAge("26");
        int newAge = divideByTwo(10);
        System.out.println("Your new age is " + newAge);
    }

    static int divideByTwo(int a){
        return a*2;
    }

    static void printGiveAge(String age){
        System.out.println("You are" + age + " years old.");
    }

    static void printGiveAge(int age){
        System.out.println("You are" + age + " years old.");
    }

    static void myFirstMethod(){
        //Methods body
        int num;

        System.out.println("Paulina");
    }

    static  void printSurname(){
        System.out.println("Nguyen");
    }
}
