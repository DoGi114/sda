public class Main {
    public static void main(String[] args) {
        StringListElement elem1 = new StringListElement("Java");
        StringListElement elem2 = new StringListElement("Jest");
        StringListElement elem3 = new StringListElement("Fajna");

        StringList lst = new StringList();
        lst.add("Java");
        lst.add("Jest");
        lst.add("Fajna");

        System.out.println("Size: " + lst.size());
        System.out.println("1 element: " + lst.get(1));
        System.out.println("2 element: " + lst.get(2));
        System.out.println("3 element: " + lst.get(3));
        lst.remove("Jest");
        System.out.println("1 element: " + lst.get(1));
        System.out.println("2 element: " + lst.get(2));
//        System.out.println("3 element: " + lst.get(3)); //NPE because there're only 2 elements now
    }
}
