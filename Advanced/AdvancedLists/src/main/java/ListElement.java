public interface ListElement {
    ListElement getNext();
    void setNext(ListElement item);
    String getValue();
    void setValue(String value);
}
