public class StringList {
    private ListElement first;
    private ListElement last;
    private ListElement curr;

    private static long size = 0;

    public StringList(){
        curr = null;
    }

    public ListElement head(){
        curr = first;
        return first;
    }

    public ListElement tail(){
        curr = last;
        return last;
    }

    public void add(String item){
        if(curr == null){
            curr = new StringListElement(item);
            first = curr;
        }else{
            StringListElement newElem = new StringListElement(item);
            curr.setNext(newElem);
            curr = newElem;
            last = curr;
        }
        size++;
    }

    public long size(){
        return size;
    }

    public void remove(String item){
        ListElement prev = null;
        curr = first;
        while(curr != null){
            if(curr.getValue().equals(item)){
                if(prev == null) {
                    first = curr.getNext();
                    size--;
                }else{
                    curr = curr.getNext();
                    prev.setNext(curr);
                }
            }
            prev = curr;
            curr = curr.getNext();
        }
    }

    public String get(int idx){
        curr = first;

        for(int i = 0; i < idx - 1; i++){
            curr = curr.getNext();
        }

        return curr.getValue();
    }
}
