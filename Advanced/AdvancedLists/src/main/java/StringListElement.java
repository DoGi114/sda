public class StringListElement implements ListElement {
    private String item;
    private ListElement next;

    public StringListElement(String item) {
        this.item = item;
        next = null;
    }

    @Override
    public ListElement getNext() {
        return next;
    }

    @Override
    public void setNext(ListElement item) {
        this.next = item;
    }

    @Override
    public String getValue() {
        return this.item;
    }

    @Override
    public void setValue(String value) {
        this.item = value;
    }
}
