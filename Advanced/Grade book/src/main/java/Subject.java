import java.util.ArrayList;
import java.util.List;

public class Subject {
    private final String name;
    private final List<Integer> grades;

    public Subject(String name){
        this.name = name;
        grades = new ArrayList<>();
    }

    public void addGrade(int grade){
        grades.add(grade);
    }

    public String getName() {
        return name;
    }

    public List<Integer> getGrades() {
        return grades;
    }

    public int getAverage(){
        int sum = 0;
        for(int grade : grades){
            sum += grade;
        }

        return sum/grades.size();
    }
}
