import java.util.ArrayList;
import java.util.List;

public class GradeBook {
    private final List<Subject> subjects;

    public GradeBook(){
        subjects = new ArrayList<>();
    }

    public void addSubject(Subject subject) throws Exception {
        for(Subject s : subjects){
            if(subject.getName().toLowerCase().equals(s.getName().toLowerCase())){
                throw new Exception("Subject is already on the list!");
            }
        }
        subjects.add(subject);
    }

    public List<Subject> getSubjects(){
        return subjects;
    }

    public Subject getSubject(Subject subject){
        for(Subject s : subjects){
            if(s.getName().equals(subject.getName())){
                return s;
            }
        }
        return null;
    }

    public Subject getSubject(String name){
        for(Subject s : subjects){
            if(s.getName().equals(name)){
                return s;
            }
        }
        return null;
    }

    public int getAverage(){
        int sum = 0;
        for(Subject s : subjects){
            sum += s.getAverage();
        }
        return sum/subjects.size();
    }
}
