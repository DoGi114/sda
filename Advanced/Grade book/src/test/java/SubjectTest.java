import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class SubjectTest {

    private static Subject english;

    @BeforeAll
    public static void setUp(){
        english = new Subject("English");
        english.addGrade(5);
        english.addGrade(4);
        english.addGrade(2);
    }

    @Test
    public void ifAverageIsCorrect(){
        assertThat(english.getAverage()).isEqualTo(3);
    }

    @Test
    public void ifNameIsValid(){
        assertThat(english.getName()).isNotEmpty();
    }

    @Test
    public void ifAddGradeIsValid(){
        english.addGrade(4);
        assertThat(english.getGrades().size()).isEqualTo(4);
    }

}
