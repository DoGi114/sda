import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.*;

public class GradeBookTest {

    private GradeBook gradebook;

    @BeforeEach
    public void setUp(){
        gradebook = new GradeBook();
        Subject english = new Subject("English");
        english.addGrade(5);
        english.addGrade(3);
        Subject math = new Subject("Math");
        math.addGrade(2);
        try {
            gradebook.addSubject(english);
            gradebook.addSubject(math);
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }

    @Test
    public void ifSizeIsValid(){
        assertThat(gradebook.getSubjects().size()).isEqualTo(2);
    }


    @Test
    public void ifSubjectIsAlreadyOnTheList(){
        Exception exception = Assertions.assertThrows(Exception.class, () -> {
            gradebook.addSubject(new Subject("English"));
        });
        assertThat(exception).hasMessage("Subject is already on the list!");
        assertThat(gradebook.getSubjects().size()).isEqualTo(2);
    }

    @Test
    public void ifAverageIsValid(){
        assertThat(gradebook.getAverage()).isEqualTo(3);
    }

    @Test
    public void ifAddingSubjectIsValid(){
        try {
            gradebook.addSubject(new Subject("Polish"));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
        assertThat(gradebook.getSubjects().size()).isEqualTo(3);
    }
}
